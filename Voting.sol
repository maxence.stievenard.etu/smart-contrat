// SPDX-License-Identifier: GPL-3.0
pragma solidity >=0.7.0 <0.9.0;

import "@openzeppelin/contracts/access/Ownable.sol";

// Contrat pour créer un système de vote intelligent

contract Voting is Ownable {

    // Permet de stocker les informations sur les votants 
    mapping(address => Voter) public voters;
    Proposal[] public proposals;
    WorkflowStatus public workflowStatus;

    struct Voter {
        bool isRegistered;
        bool hasVoted;
        uint votedProposalId;
    }

    struct Proposal {
        string description;
        uint voteCount;
    }

    enum WorkflowStatus {
        RegisteringVoters,
        ProposalsRegistrationStarted,
        ProposalsRegistrationEnded,
        VotingSessionStarted,
        VotingSessionEnded,
        VotesTallied
    }
    //Evenement permettant de notifier que le votant à bien été inscrit
    event VoterRegistered(address voterAddress);

    //Evenement permettant de notifier qu'un changement à été émis sur le vote
    event WorkflowStatusChange(
        WorkflowStatus previousStatus,
        WorkflowStatus newStatus
    );

    //Evenement permettant de notifier qu'une nouvelle proposition a été ajoutée
    event ProposalRegistered(uint proposalId);

    //Evenement permettant de notifier qu'un vote à été pris en compte 
    event Voted(address voter, uint proposalId);

    // Fonction permettant d'inscrire un votant
    function registerVoter(address _voterAddress) public onlyOwner {
        require(
            workflowStatus == WorkflowStatus.RegisteringVoters,
            "Impossible d'inscrire le votant"
        );
        require(
            !voters[_voterAddress].isRegistered,
            "Le votant est bien enregistre"
        );

        voters[_voterAddress] = Voter({
            isRegistered: true,
            hasVoted: false,
            votedProposalId: 0
        });

        emit VoterRegistered(_voterAddress);
    }

    //Fonction permettant à l'administrateur d'enregistrer les différentes propositions pour le vote
    function startProposalsRegistration() public onlyOwner {
        workflowStatus = WorkflowStatus.ProposalsRegistrationStarted;

        emit WorkflowStatusChange(
            WorkflowStatus.RegisteringVoters,
            workflowStatus
        );
    }

    //Fonction permettant à un votant d'enregistrer une proposition de vote
    function registerProposal(string memory _description) public {
        require(
            workflowStatus == WorkflowStatus.ProposalsRegistrationStarted,
            "Impossible d'enregistrer la proposition"
        );
        require(
            voters[msg.sender].isRegistered,
            "Seul les votants enregistres peuvent soumettre une porposition"
        );

        proposals.push(Proposal({description: _description, voteCount: 0}));

        emit ProposalRegistered(proposals.length - 1);
    }

    //Fonction permettant à l'administrateur de déclancher la session de vote
    function startVotingSession() public onlyOwner {
        workflowStatus = WorkflowStatus.VotingSessionStarted;

        emit WorkflowStatusChange(
            WorkflowStatus.ProposalsRegistrationEnded,
            workflowStatus
        );
    }

    //Fonction permettant le vote
    function vote(uint _proposalId) public {
        require(
            workflowStatus == WorkflowStatus.VotingSessionStarted,
            "CImpossible de voter"
        );
        require(
            voters[msg.sender].isRegistered,
            "Seul les votants enregistres peuvent voter"
        );
        require(!voters[msg.sender].hasVoted, "Le votant a deja vote");
        require(_proposalId < proposals.length, "Mauvais identifiant de proposition");

        voters[msg.sender].hasVoted = true;
        voters[msg.sender].votedProposalId = _proposalId;

        proposals[_proposalId].voteCount++;

        emit Voted(msg.sender, _proposalId);
    }

    //Fonction permettant à l'administrateur de fermer la session de vote
    function endVotingSession() public onlyOwner {
        workflowStatus = WorkflowStatus.VotesTallied;

        emit WorkflowStatusChange(
            WorkflowStatus.VotingSessionEnded,
            workflowStatus
        );
    }

    //Fonction permettant d'obtenir le gagnant du vote
    function getWinner() public view returns (uint winningProposalId) {
        require(
            workflowStatus == WorkflowStatus.VotesTallied,
            "Immossible d'obtenir le gagant"
        );

        uint winningVoteCount = 0;
        for (uint i = 0; i < proposals.length; i++) {
            if (proposals[i].voteCount > winningVoteCount) {
                winningVoteCount = proposals[i].voteCount;
                winningProposalId = i;
            }
        }
    }

    modifier onlyAdmin() {
        require(owner() == msg.sender, "Seul les administrateurs peuvent appeler la fonction");
        _;
    }

    // Fonction pour révoquer le droit de vote d'un votant
    function revokeVoterRights(address voterAddress) public onlyAdmin {
        require(workflowStatus == WorkflowStatus.RegisteringVoters, "Le droit de vote ne peut seulement etre revoque pendant un vote");
        require(voters[voterAddress].isRegistered, "Le votant nest pas enregistre");
        
        voters[voterAddress].isRegistered = false;
        voters[voterAddress].hasVoted = false;
        voters[voterAddress].votedProposalId = 0;

        emit VoterRegistered(voterAddress);
    }

    // Fonction pour voir les résultats
    function viewResults() public view returns (Proposal[] memory) {
        require(workflowStatus == WorkflowStatus.VotesTallied, "Les resultats ne peuvent etre vu qu apres la fin d un vote");
        return proposals;
    }
}
