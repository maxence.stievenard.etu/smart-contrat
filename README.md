# Rendu Smart Contrat BUT 3 Informatique

## STIEVENARD Maxence - BUT 3 Informatique - Groupe L (Réseaux)

N'ayant pas eu à faire la DAPP ainsi que le déploiement de celle-ci, je vous laisse dans ce repository le fichier Voting.sol seul afin de vous faciliter la compréhension.

J'ai également ajouté deux fonctionnalitées à ce système de vote : 

- Le fait de pouvoir révoquer les permissions de vote d'un votant
- Et le fait d'afficher les résultats du vote après qu'il soit terminé

Bien cordialement
